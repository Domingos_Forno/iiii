﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quadrantes_2.Models
{
    public partial class Resultado
    {
        public int Id { get; set; }
        [Required]
        public float ResFinal { get; set; }
        public int Idtecnica { get; set; }

        public Tecnica IdtecnicaNavigation { get; set; }
    }
}
