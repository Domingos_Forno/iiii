﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Quadrantes_2.Models
{
    public partial class SIAContext : DbContext
    {
        public SIAContext()
        {
        }

        public SIAContext(DbContextOptions<SIAContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Administrador> Administrador { get; set; }
        public virtual DbSet<Classificacao> Classificacao { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<Quadrante> Quadrante { get; set; }
        public virtual DbSet<Resultado> Resultado { get; set; }
        public virtual DbSet<Tecnica> Tecnica { get; set; }
        public virtual DbSet<Utilizador> Utilizador { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=SIA;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Administrador>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Classificacao>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");
            });

            modelBuilder.Entity<Item>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Idclassificacao).HasColumnName("IDClassificacao");

                entity.Property(e => e.Idquadrante).HasColumnName("IDQuadrante");

                entity.HasOne(d => d.IdclassificacaoNavigation)
                    .WithMany(p => p.Item)
                    .HasForeignKey(d => d.Idclassificacao)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Item__IDClassifi__34C8D9D1");

                entity.HasOne(d => d.IdquadranteNavigation)
                    .WithMany(p => p.Item)
                    .HasForeignKey(d => d.Idquadrante)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Item__IDQuadrant__33D4B598");
            });

            modelBuilder.Entity<Quadrante>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Idtecnica).HasColumnName("IDTecnica");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdtecnicaNavigation)
                    .WithMany(p => p.Quadrante)
                    .HasForeignKey(d => d.Idtecnica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Quadrante__IDTec__2F10007B");
            });

            modelBuilder.Entity<Resultado>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Idtecnica).HasColumnName("IDTecnica");

                entity.HasOne(d => d.IdtecnicaNavigation)
                    .WithMany(p => p.Resultado)
                    .HasForeignKey(d => d.Idtecnica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Resultado__IDTec__2C3393D0");
            });

            modelBuilder.Entity<Tecnica>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Ativo)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Idutilizador).HasColumnName("IDUtilizador");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdutilizadorNavigation)
                    .WithMany(p => p.Tecnica)
                    .HasForeignKey(d => d.Idutilizador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Tecnica__IDUtili__29572725");
            });

            modelBuilder.Entity<Utilizador>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
        }
    }
}
