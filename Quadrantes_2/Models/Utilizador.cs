﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quadrantes_2.Models
{
    public partial class Utilizador
    {
        public Utilizador()
        {
            Tecnica = new HashSet<Tecnica>();
        }

        public int Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public bool Ativo { get; set; }

        public ICollection<Tecnica> Tecnica { get; set; }
    }
}
