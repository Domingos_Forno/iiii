﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quadrantes_2.Models
{
    public partial class Classificacao
    {
        public Classificacao()
        {
            Item = new HashSet<Item>();
        }

        public int Id { get; set; }
        [Required]
        public float Valor { get; set; }

        public ICollection<Item> Item { get; set; }
    }
}
