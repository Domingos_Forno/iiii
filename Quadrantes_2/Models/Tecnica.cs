﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quadrantes_2.Models
{
    public partial class Tecnica
    {
        public Tecnica()
        {
            Quadrante = new HashSet<Quadrante>();
            Resultado = new HashSet<Resultado>();
        }

        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }

        public int Idutilizador { get; set; }
        public bool? Ativo { get; set; }

        public Utilizador IdutilizadorNavigation { get; set; }
        public ICollection<Quadrante> Quadrante { get; set; }
        public ICollection<Resultado> Resultado { get; set; }
    }
}
