﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quadrantes_2.Models
{
    public partial class Quadrante
    {
        public Quadrante()
        {
            Item = new HashSet<Item>();
        }

        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }

        public int Idtecnica { get; set; }

        public Tecnica IdtecnicaNavigation { get; set; }
        public ICollection<Item> Item { get; set; }
    }
}
