﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quadrantes_2.Models
{
    public partial class Item
    {
        public int Id { get; set; }
        [Required]
        public string Descricao { get; set; }

        public int Idquadrante { get; set; }
        public int Idclassificacao { get; set; }

        public Classificacao IdclassificacaoNavigation { get; set; }
        public Quadrante IdquadranteNavigation { get; set; }
    }
}
