﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using Quadrantes_2.Models;

namespace Quadrantes_2.Controllers
{
    public class UtilizadorsController : Controller
    {
        private readonly SIAContext _context;
        private static int Tecnicaatual = -1;
        private static int flag = 0;
        private static List<int> rm = new List<int>();

        public UtilizadorsController(SIAContext context)
        {
            _context = context;
        }


        public List<Utilizador> getuser()
        {
            return _context.Utilizador.Where(x => x.Id == Convert.ToInt16(HttpContext.Session.GetString("Login"))).ToList(); ;
        }

        // GET: Utilizadors
        public IActionResult Index()
        {
            verificatecnicas();

            var listatecnicas = _context.Tecnica.ToList();
            var listautilizadores = _context.Utilizador.ToList();
            ViewBag.username = getuser()[0].Username;
            ViewBag.id = getuser()[0].Id;

            return View(_context.Tecnica);
        }

        // GET: Utilizadors/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var utilizador = await _context.Utilizador
                .FirstOrDefaultAsync(m => m.Id == id);
            if (utilizador == null)
            {
                return NotFound();
            }

            return View(utilizador);
        }


        // GET: Utilizadors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var utilizador = await _context.Utilizador.FindAsync(id);
            if (utilizador == null)
            {
                return NotFound();
            }
            return View(utilizador);
        }

        // POST: Utilizadors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Username,Email,Password")] Utilizador utilizador)
        {
            if (id != utilizador.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                utilizador.Ativo = true;
                try
                {

                    if (_context.Utilizador.Where(x => x.Username == utilizador.Username).ToList().Count() == 0)
                    {
                        _context.Update(utilizador);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        ModelState.AddModelError("ErroU", "Username already exists, please enter new");
                        return View(utilizador);
                    }

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UtilizadorExists(utilizador.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(utilizador);
        }



        private bool UtilizadorExists(int id)
        {
            return _context.Utilizador.Any(e => e.Id == id);
        }


        public IActionResult Logout()
        {
            HttpContext.Session.Remove("Login");
            return RedirectToAction("Login", "Home");
        }

        public IActionResult CreateTecnica()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateTecnica([Bind("Nome")] Tecnica tecnica)
        {
            if (ModelState.IsValid)
            {
                tecnica.Idutilizador = Convert.ToInt16(HttpContext.Session.GetString("Login"));
                tecnica.Ativo = true;
                _context.Add(tecnica);
                await _context.SaveChangesAsync();
                Tecnicaatual = tecnica.Id;
                return RedirectToAction(nameof(CreateQuadrante));
            }
            return View(tecnica);
        }

        public IActionResult CreateQuadrante()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateQuadrante(string nome1, string nome2,
            string nome3, string nome4)
        {
            if (ModelState.IsValid)
            {
                Quadrante quadrante1 = new Quadrante();
                quadrante1.Nome = nome1;
                quadrante1.Idtecnica = Tecnicaatual;

                Quadrante quadrante2 = new Quadrante();
                quadrante2.Nome = nome2;
                quadrante2.Idtecnica = Tecnicaatual;

                Quadrante quadrante3 = new Quadrante();
                quadrante3.Nome = nome3;
                quadrante3.Idtecnica = Tecnicaatual;

                Quadrante quadrante4 = new Quadrante();
                quadrante4.Nome = nome4;
                quadrante4.Idtecnica = Tecnicaatual;


                _context.AddRange(quadrante1, quadrante2, quadrante3, quadrante4);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(CreateItem));
            }
            return View();
        }


        public IActionResult CreateItem()
        {
            var aux = _context.Quadrante.Where(x => x.Idtecnica == Tecnicaatual).ToList();

            foreach(var item in rm)
            {
                var arm = aux.Find(x => x.Id == item);
                aux.Remove(arm);
            }

            ViewData["Quadrante"] = new SelectList(aux, "Id", "Nome");
            ViewData["Classificacao"] = new SelectList(_context.Classificacao.ToList(), "Id", "Valor");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateItem(int Idq, string d1, int c1, string d2, int c2, string d3, int c3, string d4, int c4, string d5, int c5, string d6, int c6, string d7, int c7)
        {

            rm.Add(Idq);

            Item novo = new Item();
            List<string> desc = new List<string> { d1, d2, d3, d4, d5, d6, d7 };
            List<int> clas = new List<int> { c1, c2, c3, c4, c5, c6, c7 };

            foreach (var item in desc)
            {
                foreach (var item2 in clas)
                {
                    if (item != null && item2 != 0)
                    {
                        novo.Descricao = item;
                        novo.Idclassificacao = item2;
                        novo.Idquadrante = Idq;

                        _context.Item.Add(novo);
                    }
                }
            }

            flag++;

            await _context.SaveChangesAsync();

            if (flag < 4)
            {
                return RedirectToAction(nameof(CreateItem));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }

        public IActionResult ListMyTechniques()
        {
            ViewBag.id = getuser()[0].Id;
            var listatecnicas = _context.Tecnica.ToList();
            var listautilizadores = _context.Utilizador.ToList();
            return View(_context.Tecnica);
        }

        public async Task<IActionResult> AtivarDesativarTecnicas(int id)
        {
            var tecnica = await _context.Tecnica.FindAsync(id);
            if (tecnica.Ativo == true)
            {
                tecnica.Ativo = false;
            }
            else
            {
                tecnica.Ativo = true;
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(ListMyTechniques));
        }

        public IActionResult ListDetailsQuadrantes(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Quadrantes = _context.Quadrante.Where(x => x.Idtecnica == id).ToList(); ;

            if (Quadrantes == null)
            {
                return NotFound();
            }

            return View(Quadrantes);
        }

        public IActionResult ListDetailsItems(int? id)
        {
            var aux3 = _context.Tecnica.ToList();
            var aux2 = _context.Quadrante.ToList();
            var aux = _context.Classificacao.ToList();

            if (id == null)
            {
                return NotFound();
            }

            var Items = _context.Item.Where(x => x.Idquadrante == id).ToList(); ;


            if (Items == null)
            {
                return NotFound();
            }

            return View(Items);
        }

        public IActionResult LoadComputeResult(int id)
        {
            var aux = _context.Tecnica.ToList();
            var aux2 = _context.Quadrante.ToList();
            var aux3 = _context.Item.ToList();
            var aux4 = _context.Classificacao.ToList();


            var existeRes = _context.Resultado.Where(x => x.Idtecnica == id).ToList();
           

            if (existeRes.Count() == 0)
            {
                //calcular
                var tecnica = _context.Tecnica.Where(x => x.Id == id).ToList();
                var quadrantes = tecnica[0].Quadrante.ToList();
                int totalitems = 0;
                float resf = 0;

                Resultado res = new Resultado();
                foreach(var item in quadrantes)
                {
                    var items = item.Item.ToList();
                    totalitems += items.Count();
                    foreach(var item2 in items)
                    {
                        resf += item2.IdclassificacaoNavigation.Valor;
                    }
                }

                res.Idtecnica = id;
                res.ResFinal = (resf/totalitems);

                _context.Resultado.Add(res);
                _context.SaveChanges();
                return View(res);
            }
            else
            {
                //load
                return View(existeRes[0]);
            }

        }

        public void verificatecnicas()
        {
            var aux = _context.Tecnica.ToList();
            var aux2 = _context.Quadrante.ToList();
            var aux3 = _context.Item.ToList();
            var aux4 = _context.Classificacao.ToList();

            var apagar = aux.Where(x => x.Quadrante.ToList().Count() < 4).ToList();

            var apagar2 = aux2.Where(x => x.Item.ToList().Count < 1).ToList();

            if (apagar.Count() != 0 || apagar2.Count() !=0)
            {
                foreach(var item in apagar)
                {
                    var armI = _context.Item.Where(x => x.IdquadranteNavigation.IdtecnicaNavigation.Id == item.Id).ToList();
                    _context.RemoveRange(armI);

                    var armQ = _context.Quadrante.Where(x => x.IdtecnicaNavigation.Id == item.Id).ToList();
                    _context.RemoveRange(armQ);

                    var armR = _context.Resultado.Where(x => x.IdtecnicaNavigation.Id == item.Id).ToList();
                    _context.RemoveRange(armR);

                    var armT = _context.Tecnica.Where(x => x.Id == item.Id).ToList();
                    _context.RemoveRange(armT);

                    _context.SaveChanges();
                }

                foreach (var item in apagar2)
                {
                    var armI = _context.Item.Where(x => x.IdquadranteNavigation.IdtecnicaNavigation.Id == item.Idtecnica).ToList();
                    _context.RemoveRange(armI);

                    var armQ = _context.Quadrante.Where(x => x.IdtecnicaNavigation.Id == item.Idtecnica).ToList();
                    _context.RemoveRange(armQ);

                    var armR = _context.Resultado.Where(x => x.IdtecnicaNavigation.Id == item.Idtecnica).ToList();
                    _context.RemoveRange(armR);

                    var armT = _context.Tecnica.Where(x => x.Id == item.Idtecnica).ToList();
                    _context.RemoveRange(armT);

                    _context.SaveChanges();
                }
            }         

        }
    }
}
