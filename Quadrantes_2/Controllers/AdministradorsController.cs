﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Quadrantes_2.Models;

namespace Quadrantes_2.Controllers
{
    public class AdministradorsController : Controller
    {
        private readonly SIAContext _context;

        public AdministradorsController(SIAContext context)
        {
            _context = context;
        }

        public List<Utilizador> getuser()
        {
            return _context.Utilizador.Where(x => x.Id == Convert.ToInt16(HttpContext.Session.GetString("Login"))).ToList(); ;
        }

        // GET: Administradors
        public IActionResult Index()
        {
            ViewBag.username = getuser()[0].Username;
            ViewBag.id = getuser()[0].Id;
            return View();
        }


        private bool AdministradorExists(int id)
        {
            return _context.Administrador.Any(e => e.Id == id);
        }


        public IActionResult Logout()
        {
            HttpContext.Session.Remove("Login");
            return RedirectToAction("Login", "Home");
        }


        public IActionResult ListUsers()
        {
            var listatecnicas = _context.Tecnica.ToList();
            var listautilizadores = _context.Utilizador.ToList();
            return View(_context.Utilizador);
        }

        public IActionResult ListTechniques()
        {
            var listatecnicas = _context.Tecnica.ToList();
            var listautilizadores = _context.Utilizador.ToList();
            return View(_context.Tecnica);
        }

        public async Task<IActionResult> AtivarDesativarUser(int id)
        {
            var user = await _context.Utilizador.FindAsync(id);
            if (user.Ativo)
            {
                user.Ativo = false;
            }
            else
            {
                user.Ativo = true;
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(ListUsers));
        }

        public async Task<IActionResult> AtivarDesativarTecnicas(int id)
        {
            var tecnica = await _context.Tecnica.FindAsync(id);
            if (tecnica.Ativo==true)
            {
                tecnica.Ativo = false;
            }
            else
            {
                tecnica.Ativo = true;
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(ListTechniques));
        }

    }
}
