﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using Quadrantes_2.Models;

namespace Quadrantes_2.Controllers
{
    public class HomeController : Controller
    {

        private readonly SIAContext _context;

        public HomeController(SIAContext context)
        {
            _context = context;
        }

        public IActionResult Register()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([Bind("Id,Username,Email,Password")] Utilizador utilizador, string Con)
        {
            if (ModelState.IsValid)
            {

                if (utilizador.Password == Con)
                {
                    if (_context.Utilizador.Where(x => x.Email == utilizador.Email).ToList().Count() == 0)
                    {
                        if (_context.Utilizador.Where(x => x.Username == utilizador.Username).ToList().Count() == 0)
                        {
                            _context.Add(utilizador);
                            await _context.SaveChangesAsync();
                            SendEmail(utilizador, 0);
                            return RedirectToAction(nameof(Login));
                        }
                        else
                        {
                            ModelState.AddModelError("ErroU", "Username already exists, please enter new");
                            return View(utilizador);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("ErroM", "Email already exists, please enter new");
                        return View(utilizador);
                    }
                }
                else
                {
                    ModelState.AddModelError("ErroP", "Passwords are not the same, please enter again");
                    return View(utilizador);
                }
            }
            return View(utilizador);
        }



        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login([Bind("Email,Password")] Utilizador utilizador)
        {
            var verificar1 = _context.Utilizador.Where(x => x.Email == utilizador.Email).Where(x => x.Password == utilizador.Password).ToList();
            var verificar2 = _context.Administrador.Where(x => x.Email == utilizador.Email).Where(x => x.Password == utilizador.Password).ToList();

            if (verificar1.Count() == 0 && verificar2.Count() == 0)
            {
                ModelState.AddModelError("ErroE", "Wrong email or password");
                return View(utilizador);
            }

            else if (verificar1.Count() != 0)
            {
                if (verificar1[0].Ativo == false)
                {
                    ModelState.AddModelError("ErroA", "Your account is not active");
                    return View(utilizador);
                }
                else
                {
                    HttpContext.Session.SetString("Login", Convert.ToString(verificar1[0].Id));
                    return RedirectToAction("Index","Utilizadors");
                }
            }
            else
            {
                HttpContext.Session.SetString("Login", Convert.ToString(verificar2[0].Id));
                return RedirectToAction("Index","Administradors");
            }
        }

        public async Task<IActionResult> ConfirmEmail(string userId)
        {
            if (userId == null)
            {
                //erro
            }

            var utilizador = _context.Utilizador.Where(x => x.Id == Convert.ToInt64(userId)).ToList();

            if (utilizador.Count() == 0)
            {
                //n existe o utilizador retornar view erro
                return View();
            }
            else
            {
                utilizador[0].Ativo = true;
                await _context.SaveChangesAsync();
                return View();
            }
        }

        public IActionResult RecuperarPass()
        {
            return View();
        }

        [HttpPost]
        public IActionResult RecuperarPass([Bind("Email")] Utilizador utilizador)
        {
            if (_context.Utilizador.Where(x => x.Email == utilizador.Email).Where(x => x.Ativo == true).ToList().Count() != 0)
            {
                SendEmail(utilizador, 1);
                return RedirectToAction(nameof(Login));
            }
            else
            {
                ModelState.AddModelError("ErroC", "The email you entered is not associated with an account or the account is not active");
                return View(utilizador);
            }
        }

        public IActionResult NewPassword(string email)
        {
            if (email == null)
            {
                //erro
            }

            TempData["mail"] = email;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NewPassword([Bind("Password")] Utilizador utilizador, string Con)
        {
            if (utilizador.Password == Con)
            {
                var list = _context.Utilizador.Where(x => x.Email == (string)TempData["mail"]).ToList();
                if (list.Count() != 0)
                {
                    list[0].Password = utilizador.Password;
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Login));
                }
                else
                {
                    return View(utilizador);
                }

            }
            else
            {
                ModelState.AddModelError("ErroP", "Passwords are not the same, please enter again");
                return View(utilizador);
            }
        }

        /// <summary>
        /// type=0 confirmation email
        /// type=1 recuperarpassword email
        /// </summary>
        /// <param name="user"></param>
        /// <param name="type"></param>
        private void SendEmail(Utilizador user, int type)
        {
            var confirmationLink = "";

            if (type == 0)
            {
                confirmationLink = Url.Action("ConfirmEmail", "Home",
                    new { userId = user.Id }, Request.Scheme);
            }

            if (type == 1)
            {
                confirmationLink = Url.Action("NewPassword", "Home",
                   new { email = user.Email }, Request.Scheme);
            }

            var mensagem = new MimeMessage();
            mensagem.From.Add(new MailboxAddress("Quadrantes", "Quadrates@sapo.pt"));
            mensagem.To.Add(new MailboxAddress(user.Email));

            if (type == 0)
                mensagem.Subject = "Verificar a sua conta";
            if (type == 1)
                mensagem.Subject = "Recuperar password";

            var texto = "";

            if (type == 0)
                texto = "Use o seguinte link para validar a sua conta " + confirmationLink;
            if (type == 1)
                texto = "Use o seguinte link para recuperar a sua password " + confirmationLink;

            mensagem.Body = new TextPart("plain")
            {
                Text = texto
            };

            using (var client = new SmtpClient())
            {
                client.Connect("smtp.sapo.pt", 465, true);
                client.Authenticate("Quadrates@sapo.pt", "Abd17Snz12");
                client.Send(mensagem);
                client.Disconnect(true);
            }
        }
    }
}
