use master
go

create database SIA
go

use SIA
go

--DAR FIX NA BD TINHA UMA COISA ERRADA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

create table Administrador(
	ID integer identity(1,1) not null,
	Email varchar(50) not null,
	Password varchar(60) not null,

	Primary key(ID)
);

create table Utilizador(
	ID integer identity(1,1) not null,
	Username varchar(30) not null,
	Email varchar(50) not null,
	Password varchar(60) not null,
	Ativo bit default(0) not null,

	Primary key(ID)
);

create table Tecnica(
	ID integer identity(1,1) not null,
	Nome varchar(50) not null,
	IDUtilizador integer not null,
	Ativo bit default(1) not null,

	Primary key(ID),
	Foreign key(IDUtilizador) references Utilizador(ID)
);

create table Resultado(
	ID integer identity(1,1) not null,
	ResFinal float(10) not null,
	IDTecnica integer not null,

	Primary key(ID),
	Foreign key(IDTecnica) references Tecnica(ID)
);

create table Quadrante(
	ID integer identity(1,1) not null,
	Nome varchar(50) not null,
	IDTecnica integer not null,

	Primary key(ID),
	Foreign key(IDTecnica) references Tecnica(ID)
);

create table Classificacao(
	ID integer identity(1,1) not null,
	Valor float(10) not null,

	Primary key(ID)
);

create table Item(
	ID integer identity(1,1) not null,
	Descricao varchar(300) not null,
	IDQuadrante integer not null,
	IDClassificacao integer not null,

	Primary key(ID),
	Foreign key(IDQuadrante) references Quadrante(ID),
	Foreign key(IDClassificacao) references Classificacao(ID)
);